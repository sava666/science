package com.example.science.presentation.main.base;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.science.R;
import com.example.science.base.BaseFragment;
import com.example.science.base.DefaultNavigator;
import com.example.science.data.local.data.DataStore;
import com.example.science.models.retrofit.core.Model;
import com.example.science.presentation.main.calculation.CalculationFragment;
import com.example.science.room.model.MetricModel;
import com.example.science.util.resolver.base.ResolverImpl;
import com.example.science.util.resolver.base.ResolverInterface;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class HomeFragment extends BaseFragment<HomePresenter, DefaultNavigator> implements HomeContract.View<Fragment>, AdapterView.OnItemSelectedListener {

    @Inject
    DataStore dataStore;

    @Inject
    ResolverInterface resolverInterface;

    @BindView(R.id.metric_spinner)
    Spinner metrics;

    @BindView(R.id.characteristic_spinner)
    Spinner characteristic;

    @BindView(R.id.formula)
    TextView formula;

    @BindView(R.id.description)
    TextView description;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_main;
    }

    @Override
    protected int getToolbarResource() {
        return R.layout.toolbar_main;
    }

    @Override
    protected void onViewReady(Bundle _savedInstanceState) {
        requirePresenter().getMetricsList();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideprogress() {

    }

    @Override
    public void showMetrics(String[] metricModels) {

        ArrayAdapter<String> metricsAdapter = new ArrayAdapter<String>(requireActivity(),android.R.layout.simple_spinner_item, metricModels);
        metricsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        metrics.setAdapter(metricsAdapter);
        metrics.setOnItemSelectedListener(this);

    }

    @Override
    public void showCharacterstics(String[] characteristicModels) {

        ArrayAdapter<String> characteristicAdapter= new ArrayAdapter<String>(requireActivity(),android.R.layout.simple_spinner_item, characteristicModels);
        characteristicAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        characteristic.setAdapter(characteristicAdapter);
        characteristic.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        Spinner spinner=(Spinner)adapterView;
        if(spinner.getId()==R.id.characteristic_spinner)
        {
            dataStore.setCharacteristicTitle(spinner.getSelectedItem().toString());
            String cypher = requirePresenter().getCypher(spinner.getSelectedItem().toString());
            setupFormula(cypher);
            setupDescription(cypher);

        }
        else {
            requirePresenter().getCharacteristicsList(i+1);
        }

    }

    private void setupDescription(String cypher) {
        description.setText("");
        String descriptionText = requireContext().getResources().getString(R.string.attribute_x)+
                requireContext().getResources().getString(R.string.enter)+
                resolverInterface.resolveDescription(cypher, requireContext());
        description.setText(descriptionText);
    }

    private void setupFormula(String cypher) {
        formula.setText("");
        String formulaText = resolverInterface.resolveFormula(cypher, requireActivity());
        formula.setText(formulaText);
        dataStore.setFormula(formulaText);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    @OnClick(R.id.button_calculate)
    public void calculate(){
        getNavigator().openScreen(CalculationFragment.class, true, null, false);
    }
}
