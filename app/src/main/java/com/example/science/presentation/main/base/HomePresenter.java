package com.example.science.presentation.main.base;

import androidx.annotation.NonNull;

import com.example.science.room.model.CharacteristicModel;
import com.example.science.room.model.MetricModel;

import java.util.List;

import javax.inject.Inject;

public class HomePresenter extends HomeContract.Presenter {

    private List<CharacteristicModel> characteristicModels;

    @Inject
    public HomePresenter(@NonNull HomeContract.View _view, @NonNull HomeContract.AbsModel _model) {
        super(_view, _model);
    }

    @Override
    void showError(Throwable _throwable) {

    }

    @Override
    void getMetricsList() {
        requireModel().getMetricsList();
    }

    @Override
    void showMetricsList(List<MetricModel> metricModels) {
        requireView().showMetrics(convertListMetrics(metricModels));
    }

    @Override
    void getCharacteristicsList(int id) {
        requireModel().getCharacteristicsList(id);
    }

    @Override
    void showCharacteristicsList(List<CharacteristicModel> characteristicModels) {
        this.characteristicModels = characteristicModels;
        requireView().showCharacterstics(convertListCharacteristics(characteristicModels));
    }

    @Override
    String getCypher(String name) {
        /*CharacteristicModel characteristicModel = characteristicModels.stream().filter(characteristicModelNew -> name.equals(characteristicModelNew.getCharacteristic()))
                .findAny()
                .orElse(null);*/

        String cypher="";

        for (int i =0; i<characteristicModels.size(); i++){
            if(characteristicModels.get(i).getCharacteristic().equals(name)){
                cypher = characteristicModels.get(i).getSypher();
            }
        }
        return cypher;
    }

    private String[]convertListMetrics(List<MetricModel>metricModels){
        String[] result = new String[metricModels.size()];

        for (int i=0; i<metricModels.size();i++){
            result[i]=metricModels.get(i).getMetricName();
        }
        return result;
    }

    private String[]convertListCharacteristics(List<CharacteristicModel> characteristicModels){
        String[] result = new String[characteristicModels.size()];

        for (int i=0; i<characteristicModels.size();i++){
            result[i]=characteristicModels.get(i).getCharacteristic();
        }
        return result;
    }
}
