package com.example.science.presentation.splash;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.example.science.R;
import com.example.science.base.BaseActivity;
import com.example.science.base.DefaultNavigator;
import com.example.science.presentation.main.MainActivity;

public class SplashActivity extends BaseActivity<SplashPresenter, DefaultNavigator> implements SplashContract.View<Activity> {


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }

    @Override
    public void onCreate(@Nullable Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
        getNavigator().openScreen(MainActivity.class, true);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showError(Throwable _throwable) {

    }
}
