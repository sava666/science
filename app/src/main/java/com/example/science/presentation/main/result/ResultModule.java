package com.example.science.presentation.main.result;

import com.example.science.base.BaseFragment;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ResultModule {

    @Binds
    abstract ResultContract.Presenter providePresenter(final ResultPresenter resultPresenter);

    @Binds
    abstract ResultContract.View provideView(final ResultFragment resultFragment);

    @Binds
    abstract BaseFragment provideFragment(final ResultFragment resultFragment);
}
