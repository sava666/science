package com.example.science.presentation.main.calculation;

import android.content.Context;

import androidx.annotation.NonNull;

import com.example.science.base.BaseModel;
import com.example.science.base.BasePresenter;
import com.example.science.base.IView;

public interface CalculationContract {
    interface View<T> extends IView {

        void showProgress();
        void hideprogress();
        Context getContext();
    }
    abstract class Presenter extends BasePresenter<View, AbsModel> {

        public Presenter(@NonNull View _view, @NonNull AbsModel _model) {
            super(_view, _model);
        }
        abstract void showError(final Throwable _throwable);
    }

    abstract class AbsModel extends BaseModel<Presenter>{

    }
}
