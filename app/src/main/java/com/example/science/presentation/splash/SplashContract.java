package com.example.science.presentation.splash;

import android.content.Context;

import androidx.annotation.NonNull;

import com.example.science.base.BaseModel;
import com.example.science.base.BasePresenter;
import com.example.science.base.IView;

public interface SplashContract {
    interface View<T> extends IView {

        Context getContext();
    }

    abstract class Presenter extends BasePresenter<View, AbsModel> {

        Presenter(@NonNull View _view, @NonNull AbsModel _model) {
            super(_view, _model);
        }

       // abstract void startMainActivity();

    }

    abstract class AbsModel extends BaseModel<Presenter> {

    }
}
