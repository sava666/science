package com.example.science.presentation.main.base;

import com.example.science.base.BaseFragment;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class HomeModule {

    @Binds
    abstract HomeContract.Presenter providePresenter(final HomePresenter homePresenter);

    @Binds
    abstract HomeContract.View provideView(final HomeFragment homeFragment);

    @Binds
    abstract BaseFragment provideFragment(final HomeFragment homeFragment);
}
