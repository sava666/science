package com.example.science.presentation.main.base;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.example.science.base.BaseModel;
import com.example.science.base.BasePresenter;
import com.example.science.base.IView;
import com.example.science.presentation.main.MainContract;
import com.example.science.room.model.CharacteristicModel;
import com.example.science.room.model.MetricModel;

import java.util.List;

public interface HomeContract {
    interface View<T> extends IView{

        void showProgress();
        void hideprogress();
        Context getContext();
        void showMetrics(String[] metricModels);
        void showCharacterstics(String[] characteristicModels);
    }

    abstract class Presenter extends BasePresenter<View, AbsModel>{

        public Presenter(@NonNull View _view, @NonNull AbsModel _model) {
            super(_view, _model);
        }
        abstract void showError(final Throwable _throwable);
        abstract void getMetricsList();
        abstract void showMetricsList(List<MetricModel> metricModels);
        abstract void getCharacteristicsList(int id);
        abstract void showCharacteristicsList(List<CharacteristicModel>characteristicModels);
        abstract String getCypher(String name);
    }

    abstract class AbsModel extends BaseModel<Presenter>{

        abstract void getMetricsList();
        abstract void getCharacteristicsList(int id);

    }
}
