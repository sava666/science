package com.example.science.presentation.main;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.science.R;
import com.example.science.base.BaseActivity;
import com.example.science.base.DefaultNavigator;
import com.example.science.data.local.data.DataStore;
import com.example.science.presentation.main.base.HomeFragment;

import javax.inject.Inject;

public class MainActivity extends BaseActivity<MainPresenter, DefaultNavigator> implements MainContract.View<Activity> {

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    protected int getFragmentContainerId() {
        return R.id.container;
    }

    @Override
    public void showProgress() {
    }


    @Override
    public void hideProgress() {

    }

    @Override
    public void onCreate(@Nullable Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
        requirePresenter().insertMetric();
        getNavigator().openScreen(HomeFragment.class, false, null, true);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}
