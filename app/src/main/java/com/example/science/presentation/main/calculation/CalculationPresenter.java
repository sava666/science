package com.example.science.presentation.main.calculation;

import androidx.annotation.NonNull;

import javax.inject.Inject;

public class CalculationPresenter extends CalculationContract.Presenter{

    @Inject
    public CalculationPresenter(@NonNull CalculationContract.View _view, @NonNull CalculationContract.AbsModel _model) {
        super(_view, _model);
    }

    @Override
    void showError(Throwable _throwable) {

    }
}
