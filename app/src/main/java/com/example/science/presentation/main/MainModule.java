package com.example.science.presentation.main;

import com.example.science.base.BaseActivity;
import com.example.science.di.scope.FragmentScope;
import com.example.science.presentation.main.base.HomeContract;
import com.example.science.presentation.main.base.HomeFragment;
import com.example.science.presentation.main.base.HomeModel;
import com.example.science.presentation.main.base.HomeModule;
import com.example.science.presentation.main.calculation.CalculationContract;
import com.example.science.presentation.main.calculation.CalculationFragment;
import com.example.science.presentation.main.calculation.CalculationModel;
import com.example.science.presentation.main.calculation.CalculationModule;
import com.example.science.presentation.main.result.ResultContract;
import com.example.science.presentation.main.result.ResultFragment;
import com.example.science.presentation.main.result.ResultModel;
import com.example.science.presentation.main.result.ResultModule;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainModule {

    //result
    @FragmentScope
    @ContributesAndroidInjector(modules = ResultModule.class)
    abstract ResultFragment provideResultFragment();

    @MainScope
    @Binds
    abstract ResultContract.AbsModel provideResultModule(final ResultModel absModule);

    //calculate
    @FragmentScope
    @ContributesAndroidInjector(modules = CalculationModule.class)
    abstract CalculationFragment provideCalculationFragment();

    @MainScope
    @Binds
    abstract CalculationContract.AbsModel provideCalculationModule(final CalculationModel absModule);


    //home
    @FragmentScope
    @ContributesAndroidInjector(modules = HomeModule.class)
    abstract HomeFragment provideHomeFragment();

    @MainScope
    @Binds
    abstract HomeContract.AbsModel provideHomeModule(final HomeModel absModule);

    //main activity
    @MainScope
    @Binds
    abstract MainContract.Presenter providePresenter(final MainPresenter splashPresenter);

    @MainScope
    @Binds
    abstract MainContract.View provideView(final MainActivity splashActivity);

    @MainScope
    @Binds
    abstract BaseActivity provideActivity(final MainActivity splashActivity);
}
