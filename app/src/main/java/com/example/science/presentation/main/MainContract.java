package com.example.science.presentation.main;

import android.content.Context;

import androidx.annotation.NonNull;

import com.example.science.base.BaseModel;
import com.example.science.base.BasePresenter;
import com.example.science.base.IView;
import com.example.science.room.model.CharacteristicModel;
import com.example.science.room.model.MetricModel;

import java.util.List;

public interface MainContract {
    interface View<T> extends IView {


        void showProgress();

        void hideProgress();

        Context getContext();
    }

    abstract class Presenter extends BasePresenter<View, AbsModel> {

        Presenter(@NonNull View _view, @NonNull AbsModel _model) {
            super(_view, _model);
        }

        abstract void showError(final Throwable _throwable);

        abstract void insertMetric();


    }

    abstract class AbsModel extends BaseModel<Presenter> {

        abstract void insertMetricItem(MetricModel... metricModels);

        abstract void insertCharacteristicItem(CharacteristicModel... characteristicModels);

    }
}
