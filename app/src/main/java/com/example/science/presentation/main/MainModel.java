package com.example.science.presentation.main;


import com.example.science.domain.insert.characteristic.InsertCharacteristicRepository;
import com.example.science.domain.insert.metric.InsertMetricRepository;
import com.example.science.room.model.CharacteristicModel;
import com.example.science.room.model.MetricModel;

import javax.inject.Inject;

public class MainModel extends MainContract.AbsModel{

    private InsertMetricRepository insertMetricRepository;
    private InsertCharacteristicRepository insertCharacteristicRepository;

    @Inject
    MainModel(InsertMetricRepository insertMetricRepository, InsertCharacteristicRepository insertCharacteristicRepository){
        this.insertMetricRepository = insertMetricRepository;
        this.insertCharacteristicRepository = insertCharacteristicRepository;
    }

    @Override
    void insertMetricItem(MetricModel... metricModels) {
        insertMetricRepository.insertMetric(metricModels);
    }

    @Override
    void insertCharacteristicItem(CharacteristicModel... characteristicModels) {
        insertCharacteristicRepository.insertCharacteristic(characteristicModels);
    }
}
