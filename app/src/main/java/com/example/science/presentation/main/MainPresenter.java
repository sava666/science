package com.example.science.presentation.main;

import androidx.annotation.NonNull;

import com.example.science.data.local.data.DataStore;
import com.example.science.room.model.CharacteristicModel;
import com.example.science.room.model.MetricModel;

import javax.inject.Inject;

public class MainPresenter extends MainContract.Presenter{
    @Inject
    MainPresenter(@NonNull MainContract.View _view, @NonNull MainContract.AbsModel _model) {
        super(_view, _model);
    }

    @Inject
    DataStore dataStore;


    @Override
    void showError(Throwable _throwable) {

    }

    @Override
    void insertMetric() {
        if(isAttached()&&dataStore.getIfUpdate())
        {
            requireModel().insertMetricItem(
                    new MetricModel("Функційність"),
                    new MetricModel("Надійність"),
                    new MetricModel("Зручність"),
                    new MetricModel("Ефективність"),
                    new MetricModel("Супроводжуваність"),
                    new MetricModel("Переносність"));

            requireModel().insertCharacteristicItem(
                    new CharacteristicModel(1,"Очікувана точність реалізації функцій","Ф201"),
                    new CharacteristicModel(1,"Обчислювальна правильність","Ф202"),
                    new CharacteristicModel(1,"Точність","Ф203"),
                    new CharacteristicModel(1,"Здатність до обміну даними (заснованому  на успішних спробах користувача)","Ф302"),
                    new CharacteristicModel(1,"Запобігання псуванню даних","Ф403"),

                    new CharacteristicModel(2,"Середній час між відмовами (MTBF)","Н106"),
                    new CharacteristicModel(2,"Готовність","Н301"),
                    new CharacteristicModel(2,"Середній час простою","Н302"),
                    new CharacteristicModel(2,"Середній час відновлення","Н303"),

                    new CharacteristicModel(3,"Операційна відповідність під час використовування","З301"),
                    new CharacteristicModel(3,"Коригування помилок під час використовування","З303"),
                    new CharacteristicModel(3,"Зрозумілість повідомлень під час використовування","З305"),
                    new CharacteristicModel(3,"Період часу між операціями, у яких людиною були припущені помилки","З308"),

                    new CharacteristicModel(4,"Час відгуку (середній час до відгуку)","Е102"),
                    new CharacteristicModel(4,"Час відгуку (коефіцієнт найгіршого часу відгуку)","Е103"),
                    new CharacteristicModel(4,"Пропускна здатність","Е104"),
                    new CharacteristicModel(4,"Пропускна здатність (середній обсяг пропускної здатності)","Е105"),
                    new CharacteristicModel(4,"Пропускна здатність (коефіцієнт найгіршої пропускної здатності)","Е106"),
                    new CharacteristicModel(4,"Час виконання запиту (Середній час виконання запиту)","Е108"),
                    new CharacteristicModel(4,"Час виконання запиту (Рівень найгіршого пропускного періоду)","Е109"),
                    new CharacteristicModel(4,"Час очікування)","Е110"),
                    new CharacteristicModel(4,"Завантаженість пристроїв В/В","Е102"),
                    new CharacteristicModel(4,"Помилки В/В","Е203"),
                    new CharacteristicModel(4,"Кількість помилок пам’яті в одиницю часу","Е208"),
                    new CharacteristicModel(4,"Збалансованість використання медіа засобів","Е210"),
                    new CharacteristicModel(4,"Середня кількість помилок передачі в одиницю часу","Е212"),

                    new CharacteristicModel(5,"Ефективність аналізування відмов","С104"),
                    new CharacteristicModel(5,"Ефективність циклу змін","С201"),
                    new CharacteristicModel(5,"Тривалість реалізації змін","С202"),
                    new CharacteristicModel(5,"Складність модифікування","С203"),
                    new CharacteristicModel(5,"Відносний успіх змін","С301"),
                    new CharacteristicModel(5,"Ефективність повторного тестування","С402"),

                    new CharacteristicModel(6,"Готовність до співіснування","П301")
            );
            dataStore.setIfUpdate(false);
        }
    }


}
