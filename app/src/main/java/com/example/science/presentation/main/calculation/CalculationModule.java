package com.example.science.presentation.main.calculation;

import com.example.science.base.BaseFragment;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class CalculationModule {

    @Binds
    abstract CalculationContract.Presenter providePresenter(final CalculationPresenter calculationPresenter);

    @Binds
    abstract CalculationContract.View provideView(final CalculationFragment calculationFragment);

    @Binds
    abstract BaseFragment provideFragment(final CalculationFragment calculationFragment);

}
