package com.example.science.presentation.main.base;

import com.example.science.domain.get.characteristics.GetCharacteristicsRepository;
import com.example.science.domain.get.metrics.GetMetricsRepository;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class HomeModel extends HomeContract.AbsModel {
    private GetMetricsRepository getMetricsRepository;
    private GetCharacteristicsRepository getCharacteristicsRepository;

    @Inject
    HomeModel(GetMetricsRepository getMetricsRepository, GetCharacteristicsRepository getCharacteristicsRepository){
        this.getMetricsRepository = getMetricsRepository;
        this.getCharacteristicsRepository = getCharacteristicsRepository;
    }

    @Override
    void getMetricsList() {
        addDisposable(getMetricsRepository.getMetricsList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(metrics -> safeDelegate(() -> requirePresenter().showMetricsList(metrics)),
                        _throwable ->
                                safeDelegate(() -> requirePresenter().showError(_throwable))));
    }

    @Override
    void getCharacteristicsList(int id) {
        addDisposable(getCharacteristicsRepository.getCharacteristicList(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(characteristics -> safeDelegate(() -> requirePresenter().showCharacteristicsList(characteristics)),
                        _throwable ->
                                safeDelegate(() -> requirePresenter().showError(_throwable))));
    }
}
