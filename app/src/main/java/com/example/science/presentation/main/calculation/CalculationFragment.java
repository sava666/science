package com.example.science.presentation.main.calculation;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.science.R;
import com.example.science.base.BaseFragment;
import com.example.science.base.DefaultNavigator;
import com.example.science.data.local.data.DataStore;
import com.example.science.presentation.main.result.ResultFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class CalculationFragment extends BaseFragment<CalculationPresenter, DefaultNavigator>implements CalculationContract.View<Fragment> {
    @BindView(R.id.characteristic)
    TextView characteristic;

    @BindView(R.id.formula)
    TextView formula;

    @BindView(R.id.formula_one)
    LinearLayout formulaOne;

    @BindView(R.id.formula_two)
    LinearLayout formulaTwo;

    @BindView(R.id.formula_three)
    LinearLayout formulaThree;

    @BindView(R.id.formula_four)
    LinearLayout formulaFour;



    @BindView(R.id.ai_value)
    EditText aiValueOne;

    @BindView(R.id.ti_value)
    EditText tiValueOne;

    @BindView(R.id.ti_value_second)
    EditText tiValueSecond;

    @BindView(R.id.tai_value)
    EditText taiValue;

    @BindView(R.id.ti_value_three)
    EditText tiValueThree;

    @BindView(R.id.warning)
    TextView warningText;

    @BindView(R.id.result)
    TextView sigmaTextResult;

    @BindView(R.id.ti_value_four)
    EditText tiValueFour;

    @BindView(R.id.ai_value_four)
    EditText aiValueFour;


    @Inject
    DataStore dataStore;

    private float sigmaResult = 0;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_calculate;
    }

    @Override
    protected int getToolbarResource() {
        return R.layout.toolbar_calculate;
    }

    @Override
    public void createToolbar(View toolbar) {
        toolbar.findViewById(R.id.back_press).setOnClickListener(view -> getNavigator().navigateBack());
    }

    @Override
    protected void onViewReady(Bundle _savedInstanceState) {
        characteristic.setText(dataStore.getCharacteristicTitle());
        formula.setText(dataStore.getFormula());
        formulaResolver(dataStore.getFormulaId());
    }

    private void formulaResolver(int formulaId) {

        if(formulaId==1||formulaId==2||formulaId==7){
            setupFormulaOne();
        }
        else if(formulaId==3||formulaId==5){
            setupFormulaTwo();
        }
        else if(formulaId==4){
            setupFormulaThree();
        }
        else if(formulaId==6){
            setupFormulaFour();
        }
        else {
            defaultFormula();
        }

    }

    private void defaultFormula() {
        formulaOne.setVisibility(View.GONE);
        formulaTwo.setVisibility(View.GONE);
        formulaThree.setVisibility(View.GONE);
        formulaFour.setVisibility(View.GONE);

    }

    private void setupFormulaFour() {

        formulaOne.setVisibility(View.GONE);
        formulaTwo.setVisibility(View.GONE);
        formulaThree.setVisibility(View.GONE);
        formulaFour.setVisibility(View.VISIBLE);

    }

    private void setupFormulaThree() {
        formulaOne.setVisibility(View.GONE);
        formulaTwo.setVisibility(View.GONE);
        formulaThree.setVisibility(View.VISIBLE);
        formulaFour.setVisibility(View.GONE);

    }

    private void setupFormulaTwo() {
        formulaOne.setVisibility(View.GONE);
        formulaTwo.setVisibility(View.VISIBLE);
        formulaThree.setVisibility(View.GONE);
        formulaFour.setVisibility(View.GONE);

    }

    private void setupFormulaOne() {
        formulaOne.setVisibility(View.VISIBLE);
        formulaTwo.setVisibility(View.GONE);
        formulaThree.setVisibility(View.GONE);
        formulaFour.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideprogress() {

    }

    @OnClick(R.id.button_calculate)
    public void calculate(){

        int formulaId = dataStore.getFormulaId();

        if(formulaId==1){
            if(validation(aiValueOne, tiValueOne, tiValueOne)){
                float result =  Float.valueOf(aiValueOne.getText().toString())/Float.valueOf(tiValueOne.getText().toString());
                dataStore.setResult(result);
                getNavigator().openScreen(ResultFragment.class, true, null, false);
            }
        }
        else if(formulaId==2){
            if(validation(aiValueOne, tiValueOne, aiValueOne)){
                float result =  Float.valueOf(tiValueOne.getText().toString())/Float.valueOf(aiValueOne.getText().toString());
                dataStore.setResult(result);
                getNavigator().openScreen(ResultFragment.class, true, null, false);
            }
        }
        else if(formulaId==7){
            if(validation(aiValueOne, tiValueOne, null)){
                float result =  Float.valueOf(tiValueOne.getText().toString())*Float.valueOf(aiValueOne.getText().toString());
                dataStore.setResult(result);
                getNavigator().openScreen(ResultFragment.class, true, null, false);
            }
        }
        else if(formulaId==3){
            if(validationThree(tiValueSecond, taiValue)){
                float result =  Float.valueOf(tiValueSecond.getText().toString())/
                        (Float.valueOf(taiValue.getText().toString())+Float.valueOf(tiValueSecond.getText().toString()));
                dataStore.setResult(result);
                getNavigator().openScreen(ResultFragment.class, true, null, false);
            }
        }
        else if(formulaId==5){
            if(validation(tiValueSecond, taiValue, taiValue)){
                float result =  Float.valueOf(tiValueSecond.getText().toString())/Float.valueOf(taiValue.getText().toString());
                dataStore.setResult(result);
                getNavigator().openScreen(ResultFragment.class, true, null, false);
            }
        }
        else if(formulaId==4){
            if(validationFour(tiValueThree)){
                float result =  Float.valueOf(tiValueThree.getText().toString());
                dataStore.setResult(result);
                getNavigator().openScreen(ResultFragment.class, true, null, false);
            }
        }
        else if(formulaId==6){
            if(validationSix(aiValueFour)){
                float result =  sigmaResult/Float.valueOf(aiValueFour.getText().toString());
                dataStore.setResult(result);
                getNavigator().openScreen(ResultFragment.class, true, null, false);
            }
        }
        else {
            defaultFormula();
        }

    }

    private boolean validationSix(EditText aiValueFour) {
        if(aiValueFour.getText().toString().equals("")){
            warningText.setVisibility(View.VISIBLE);
            warningText.setText("");
            warningText.setText("Перевірте введене значення");
            return false;
        }
        else if(Float.valueOf(aiValueFour.getText().toString())==0.0||
                Float.valueOf(aiValueFour.getText().toString())==0){
            warningText.setVisibility(View.VISIBLE);
            warningText.setText("");
            warningText.setText("Ділення на нуль");
            return false;
        }
        else {
            warningText.setVisibility(View.GONE);
            return true;
        }
    }

    private boolean validationFour(EditText tiValueThree) {
        if(tiValueThree.getText().toString().equals("")){
            warningText.setVisibility(View.VISIBLE);
            warningText.setText("");
            warningText.setText("Перевірте введене значення");
            return false;
        }
        else {
            warningText.setVisibility(View.GONE);
            return true;
        }
    }

    private boolean validationThree(EditText aiValueOne, EditText tiValueOne) {
        if(aiValueOne.getText().toString().equals("")||
                tiValueOne.getText().toString().equals("")){
            warningText.setVisibility(View.VISIBLE);
            warningText.setText("");
            warningText.setText("Перевірте введені значення");
            return false;
        }
        else if(Float.valueOf(aiValueOne.getText().toString())+Float.valueOf(tiValueOne.getText().toString())==0.0){
            warningText.setVisibility(View.VISIBLE);
            warningText.setText("");
            warningText.setText("Ділення на нуль");
            return false;
        }
        else {
            warningText.setVisibility(View.GONE);
            return true;
        }

    }

    private boolean validation(EditText aiValueOne, EditText tiValueOne, EditText bottomField) {
        if(aiValueOne.getText().toString().equals("")||
            tiValueOne.getText().toString().equals("")){
            warningText.setVisibility(View.VISIBLE);
            warningText.setText("");
            warningText.setText("Перевірте введені значення");
            return false;
        }
        else if(bottomField!=null&&Float.valueOf(bottomField.getText().toString())==0.0||
                bottomField!=null&&Float.valueOf(bottomField.getText().toString())==0){
            warningText.setVisibility(View.VISIBLE);
            warningText.setText("");
            warningText.setText("Ділення на нуль");
            return false;
        }
        else {
            warningText.setVisibility(View.GONE);
            return true;
        }

    }

    @OnClick(R.id.add)
    public void add(){

        if(validationFour(tiValueFour)){
            sigmaResult = sigmaResult + Float.valueOf(tiValueFour.getText().toString());
            sigmaTextResult.setText(sigmaTextResult.getText()+ " + " + Float.valueOf(tiValueFour.getText().toString()));
        }

    }

}
