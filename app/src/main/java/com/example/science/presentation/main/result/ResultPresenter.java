package com.example.science.presentation.main.result;

import androidx.annotation.NonNull;

import javax.inject.Inject;

public class ResultPresenter extends ResultContract.Presenter {

    @Inject
    public ResultPresenter(@NonNull ResultContract.View _view, @NonNull ResultContract.AbsModel _model) {
        super(_view, _model);
    }

    @Override
    void showError(Throwable _throwable) {

    }
}
