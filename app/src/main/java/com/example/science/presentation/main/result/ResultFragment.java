package com.example.science.presentation.main.result;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.science.R;
import com.example.science.base.BaseFragment;
import com.example.science.base.DefaultNavigator;
import com.example.science.data.local.data.DataStore;

import javax.inject.Inject;

import butterknife.BindView;

public class ResultFragment extends BaseFragment<ResultPresenter, DefaultNavigator> implements ResultContract.View<Fragment> {

    @BindView(R.id.result)
    TextView result;

    @BindView(R.id.characteristic)
    TextView characteristic;

    @BindView(R.id.formula)
    TextView formula;

    @Inject
    DataStore dataStore;
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_result;
    }

    @Override
    protected int getToolbarResource() {
        return R.layout.toolbar_result;
    }

    @Override
    public void createToolbar(View toolbar) {
       toolbar.findViewById(R.id.back_press).setOnClickListener(view -> getNavigator().navigateBack());
    }

    @Override
    protected void onViewReady(Bundle _savedInstanceState) {
        result.setText("Xi = "+String.valueOf(dataStore.getResult()));
        formula.setText(dataStore.getFormula());
        characteristic.setText(dataStore.getCharacteristicTitle());
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideprogress() {

    }
}
