package com.example.science.base;


import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BaseModel<P extends IPresenter> implements IModel<P> {

    @SuppressWarnings("WeakerAccess")
    P presenter;
    private final List<Runnable> commands = new ArrayList<>();

    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @NonNull
    protected final P requirePresenter() {
        if (!hasPresenter())
            throw new IllegalStateException("Presenter is not attached to the model");
        return presenter;
    }

    @CallSuper

    public void setPresenter(@NonNull final P _presenter) {
        presenter = _presenter;
        runCommands();
    }

    @CallSuper

    public void removePresenter() {
        presenter = null;
    }

    @CallSuper

    public boolean hasPresenter() {
        return presenter != null;
    }

    protected final void safeDelegate(final Runnable cmd) {
        if (hasPresenter()) {
            cmd.run();
        } else {
            addCommand(cmd);
        }
    }

    //region Commander
    private void addCommand(final Runnable cmd) {
        commands.add(cmd);
    }

    protected void addDisposable(final Disposable _disposable) {
        compositeDisposable.add(_disposable);
    }

    protected void removeDisposable(final Disposable _disposable) {
        if (_disposable != null)
            compositeDisposable.remove(_disposable);
    }

    public void clearDisposable() {
        compositeDisposable.clear();
    }

    private void runCommands() {
        final Iterator<Runnable> iterator = commands.iterator();
        while (iterator.hasNext()) {
            safeDelegate(iterator.next());
            iterator.remove();
        }
    }

}
