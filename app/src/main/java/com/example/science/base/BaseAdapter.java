package com.example.science.base;


import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public abstract class BaseAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
    private ArrayList<T> items = new ArrayList<>();
    private ArrayList<T> itemsDuplicate = new ArrayList<>();

    public BaseAdapter() {
        setHasStableIds(true);
    }

    public void add(T object) {
        items.add(object);
        notifyDataSetChanged();
    }

    public void add(int index, T object) {
        items.add(index, object);
        notifyDataSetChanged();
    }

    public void addAll(Collection<? extends T> collection) {
        if (collection != null) {
            items.addAll(collection);
            notifyDataSetChanged();
        }
    }

    public void addAllDuplicate(Collection<? extends T> collection) {
        if (collection != null) {
            itemsDuplicate.clear();
            itemsDuplicate.addAll(collection);
            notifyDataSetChanged();
        }
    }

    public void addAllWithoutClear(Collection<? extends T> collection) {
        if (collection != null) {

            //  items.clear();
            items.addAll(collection);
            notifyDataSetChanged();
        }
    }

    public void addAll(T... items) {
        addAll(Arrays.asList(items));
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    public void remove(String object) {
        items.remove(object);
        notifyDataSetChanged();
    }

    public T getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).hashCode();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public List<T> getList() {
        return items;
    }

    public List<T> getListDuplicate() {
        return itemsDuplicate;
    }

    public void updateList(Collection<? extends T> collection) {
        items.clear();
        if (collection != null)
            items.addAll(collection);

        this.notifyDataSetChanged();

    }

    public interface ItemClick<T> {

        void onItemClicked(T item);
    }
}