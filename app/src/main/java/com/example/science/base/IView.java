package com.example.science.base;

public interface IView {
    void showError(final Throwable _throwable);
}