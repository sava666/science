package com.example.science.base;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.CallSuper;
import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.science.MyApplication;
import com.example.science.R;
import com.example.science.base.error.DefaultHttpErrorHandler;
import com.example.science.base.error.EmptyCommand;
import com.example.science.base.error.ErrorHandler;
import com.example.science.base.error.NetworkErrorHandler;
import com.example.science.base.error.UnauthorizedErrorHandler;
import com.example.science.base.error.UnknownErrorHandler;
import com.example.science.di.Injectable;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;


public abstract class BaseActivity<P extends IPresenter, N extends INavigator> extends AppCompatActivity
        implements IView, Injectable, HasAndroidInjector {

    @Inject
    DispatchingAndroidInjector<Object> fragmentDispatchingAndroidInjector;

    @Inject
    P presenter;
    @Inject
    N navigator;
    private Unbinder unbinder;

    private ErrorDialog errorDialog;

    private final ErrorHandler unauthorizedErrorHandler = new UnauthorizedErrorHandler(new EmptyCommand() {
        @Override
        public void execute() {
            onAttachDialog();
            errorDialog = new ErrorDialog(BaseActivity.this,
                    MyApplication.getInstance().getResources().getString(R.string.unauthorized_error));
            errorDialog.show();
        }
    });

    private final ErrorHandler defaultHttpErrorHandler = new DefaultHttpErrorHandler(_arg -> {
        onAttachDialog();
        errorDialog = new ErrorDialog(BaseActivity.this, _arg);
        errorDialog.show();
    });

    //TODO add retry option for network errors
    private final ErrorHandler networkErrorHandler = new NetworkErrorHandler(_arg -> {
        onAttachDialog();
        errorDialog = new ErrorDialog(BaseActivity.this, _arg);
        errorDialog.show();
    });

    private final ErrorHandler unknownErrorHandler = new UnknownErrorHandler(_arg -> {
        onAttachDialog();
        errorDialog = new ErrorDialog(BaseActivity.this, _arg);
        errorDialog.show();
    });
    private void onAttachDialog(){
        if(errorDialog!=null&&errorDialog.isShowing())
            errorDialog.dismiss();
    }

    {
        unauthorizedErrorHandler.setSuccessor(defaultHttpErrorHandler);
        defaultHttpErrorHandler.setSuccessor(networkErrorHandler);
        networkErrorHandler.setSuccessor(unknownErrorHandler);
    }

    @LayoutRes
    protected abstract int getLayoutResource();

    //by default i return -1, if the corresponding activity may show a fragment
    //then return it's containers resourceId
    @IdRes
    protected int getFragmentContainerId() {
        return -1;
    }

    protected final P requirePresenter() {
        if (presenter == null)
            throw new IllegalStateException("Presenter is not attached!!!");
        return presenter;
    }

    @SuppressWarnings("unchecked")
    protected final N getNavigator() {
        return navigator;
    }

    @SuppressWarnings("unchecked")
    @CallSuper
    @Override
    public void onCreate(@Nullable final Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
        if(getLayoutResource()!=0){
            setContentView(getLayoutResource());
        }

        unbinder = ButterKnife.bind(this);
        if (_savedInstanceState == null) {
            presenter.attach(this);
        } else {
            presenter.reAttach(this);
        }
        parseExtras(getIntent());
        onViewReady(_savedInstanceState);

    }


    protected void onViewReady(@Nullable final Bundle _savedInstanceState) {
    }

    protected void parseExtras(@Nullable final Intent _intent) {
    }

    @Nullable
    protected final Fragment getCurrentFragment() {
        if (getFragmentContainerId() == -1) return null;
        return getSupportFragmentManager().findFragmentById(getFragmentContainerId());
    }

    //    @Override
//    public AndroidInjector<Object> supportFragmentInjector() {
//        return fragmentDispatchingAndroidInjector;
//    }
    @Override
    public AndroidInjector<Object> androidInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    @Override
    public void showError(final Throwable _throwable) {
        unauthorizedErrorHandler.handleException(_throwable);
    }

    private void unbindViews() {
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }

    @CallSuper
    @Override
    protected void onDestroy() {

        unbindViews();
        presenter.detach();
        navigator.dispose();
        super.onDestroy();
    }

    @CallSuper
    @Override
    public void onBackPressed() {
        final boolean canGoBack = navigator.navigateBack();
        if (!canGoBack) super.onBackPressed();
    }
}