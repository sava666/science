package com.example.science.base;

import android.view.View;

public interface IToolbar {
    void createToolbar(final View toolbar);
}
