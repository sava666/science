package com.example.science.base;

import android.content.Context;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.science.R;

import butterknife.BindView;
import butterknife.OnClick;

public final class ErrorDialog extends BaseDialog {

    @BindView(R.id.message)
    protected TextView message;

    private String description;

    public ErrorDialog(@NonNull final Context context, @NonNull final String description) {
        super(context);
        this.description = description;
    }

    @Override
    protected final void initDialog() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.initDialog();
    }

    @Override
    protected final int getLayoutResource() {
        return R.layout.dialog_error;
    }

    @Override
    public final void onAttachedToWindow() {
        super.onAttachedToWindow();
        message.setText(description);
    }

    @OnClick(R.id.btn_ok)
    final void onOkClicked() {
        dismiss();
    }
}