package com.example.science.base.error;


import com.example.science.data.network.error.NetworkException;

public final class UnauthorizedErrorHandler extends ErrorHandler {

    public UnauthorizedErrorHandler(final Command _performAction) {
        super(_performAction);
    }

    @SuppressWarnings("unchecked")
    @Override
    public final void handleException(final Throwable _throwable) {
        super.handleException(_throwable);
        final NetworkException erpException = (NetworkException) _throwable;
        if (erpException.getKind() == NetworkException.Kind.HTTP &&
                erpException.getResponseCode() == 422) {
            performAction.execute(null);
        } else {
            if (successor != null) successor.handleException(_throwable);
        }
    }
}