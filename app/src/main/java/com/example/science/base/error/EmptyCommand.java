package com.example.science.base.error;

public abstract class EmptyCommand implements Command {

    @Override
    public void execute(final Object _arg) {
        execute();
    }

    public abstract void execute();
}
