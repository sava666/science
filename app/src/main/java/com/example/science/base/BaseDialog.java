package com.example.science.base;

import android.app.Dialog;
import android.content.Context;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseDialog extends Dialog {

    private Unbinder unbinder;

    public BaseDialog(@NonNull final Context context) {
        this(context, 0);
    }

    protected BaseDialog(@NonNull final Context context,
                         final int themeResId) {
        super(context, themeResId);
        initDialog();
    }

    @CallSuper
    protected void initDialog() {
        setContentView(getLayoutResource());
    }

    @LayoutRes
    protected abstract int getLayoutResource();

    @CallSuper
    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        unbinder = ButterKnife.bind(this);
    }

    @CallSuper
    @Override
    public void onDetachedFromWindow() {
        if (unbinder != null) unbinder.unbind();
        super.onDetachedFromWindow();
    }
}