package com.example.science.base;

import android.content.Context;
import android.view.View;

import androidx.annotation.CallSuper;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.ButterKnife;

public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {

    private T data;

    public BaseViewHolder(final View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @CallSuper
    public void bindData(final T _data) {
        data = _data;
    }

    protected final T getData() {
        return data;
    }

    protected final Context requireContext() {
        if (itemView.getContext() == null)
            throw new IllegalStateException("View " + this + " not attached to recyclerview");
        return itemView.getContext();
    }

    public void onViewAttachedToWindow() { }

    public void onViewDetachedFromWindow() { }
}
