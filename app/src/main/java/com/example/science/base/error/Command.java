package com.example.science.base.error;

public interface Command<T> {
    void execute(final T _arg);
}