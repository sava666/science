package com.example.science.base;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import com.example.science.R;
import com.example.science.data.network.error.NetworkException;

import javax.inject.Inject;

public class DefaultNavigator implements INavigator {

    private BaseActivity baseActivity;

    @Inject
    public DefaultNavigator(final BaseActivity _baseActivity) {
        baseActivity = _baseActivity;
    }

    public final <T extends Activity> void openScreen(final Class<T> _activityToOpen) {
        openScreen(_activityToOpen, false);
    }

    public final <T extends Activity> void openScreen(final Class<T> _activityToOpen,
                                                      final boolean _finishCurrent) {
        openScreen(_activityToOpen, null, null, _finishCurrent, false);
    }

    public final <T extends Activity> void openScreen(final Class<T> _activityToOpen,
                                                      final boolean _finishCurrent,
                                                      final boolean _finishAll) {
        openScreen(_activityToOpen, null, null, _finishCurrent, _finishAll);
    }

    public final <T extends Activity> void openScreen(final Class<T> _activityToOpen,
                                                      final Pair<String, Bundle> extras,
                                                      final boolean finishCurrent) {
        openScreen(_activityToOpen, extras, null, finishCurrent, false);
    }

    public final <T extends Activity> void openScreen(final Class<T> _activityToOpen,
                                                      final ActivityOptions _activityOptions,
                                                      final boolean finishCurrent) {
        openScreen(_activityToOpen, null, _activityOptions, finishCurrent, false);
    }

    public final <T extends Activity> void openScreen(final Class<T> _activityToOpen,
                                                      final Pair<String, Bundle> extras,
                                                      final ActivityOptions _activityOptions,
                                                      final boolean finishCurrent,
                                                      final boolean finishAll) {
        final Intent launchIntent = new Intent(baseActivity, _activityToOpen);
        if (extras != null) {
            launchIntent.putExtra(extras.first, extras.second);
        }
        ActivityCompat.startActivity(baseActivity, launchIntent,
                _activityOptions != null ? _activityOptions.toBundle() : null);
        if (finishAll)
            baseActivity.finishAffinity();
        else if (finishCurrent)
            baseActivity.finish();
    }

    public final <T extends Activity> void openScreenForResult(final Class<T> _activityToOpen,
                                                               final Pair<String, Bundle> _extras,
                                                               final ActivityOptions _activityOptions,
                                                               final int _requestCode) {
        final Intent launchIntent = new Intent(baseActivity, _activityToOpen);
        if (_extras != null) {
            launchIntent.putExtra(_extras.first, _extras.second);
        }
        ActivityCompat.startActivityForResult(baseActivity, launchIntent,
                _requestCode, _activityOptions != null ? _activityOptions.toBundle() : null);
    }

    public final void finishWithResult(final int _responseCode,
                                       final Pair<String, Bundle> _extras) {
        final Intent resultIntent = new Intent();
        if (_extras != null) {
            resultIntent.putExtra(_extras.first, _extras.second);
        }
        baseActivity.setResult(_responseCode, resultIntent);
        baseActivity.finish();
    }

    public final <T extends Fragment> void openScreen(final Class<T> _fragmentToShow,
                                                      final @Nullable Bundle _arguments) {
        openScreen(_fragmentToShow, true, _arguments, true);
    }

    public final <T extends Fragment> void openScreen(final Class<T> _fragmentToShow,
                                                      final boolean _addToBackStack,
                                                      final @Nullable Bundle _arguments,
                                                      final boolean replace) {
        final Fragment currentFragment = baseActivity.getSupportFragmentManager()
                .findFragmentById(baseActivity.getFragmentContainerId());
        if (currentFragment != null && currentFragment.getClass().equals(_fragmentToShow)) return;
        final Fragment fragment;
        if (_arguments == null) {
            fragment = Fragment.instantiate(baseActivity, _fragmentToShow.getName());
        } else {
            fragment = Fragment.instantiate(baseActivity, _fragmentToShow.getName(), _arguments);
        }

        final FragmentTransaction fragmentTransaction = baseActivity.getSupportFragmentManager()
                .beginTransaction();
        if (_addToBackStack) {
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
            fragmentTransaction.setCustomAnimations(R.anim.slide_in, R.anim.slide_out,
                    R.anim.anim_exit_in, R.anim.anim_exit);
        } else {
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.setCustomAnimations(R.anim.anim_not_null, R.anim.anim_not_null,
                    R.anim.anim_not_null, R.anim.anim_not_null);
        }
        if(replace){
            fragmentTransaction.replace(baseActivity.getFragmentContainerId(), fragment, fragment.getClass().getName());
            fragmentTransaction.commitAllowingStateLoss();
        }
        else {
            fragmentTransaction.add(baseActivity.getFragmentContainerId(), fragment, fragment.getClass().getName());
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

    @Override
    public final boolean navigateBack() {
        return navigateBack(false);
    }

    @Override
    public final boolean navigateBack(final boolean _closeAll) {
        final FragmentManager fragmentManager = baseActivity.getSupportFragmentManager();
        final int backStackSize = fragmentManager.getBackStackEntryCount();
        if (!_closeAll) {
            if (backStackSize == 1) {
                fragmentManager.popBackStack();
                return false;
            } else {
                return baseActivity.getSupportFragmentManager().popBackStackImmediate();
            }
        }
        for (int i = 0; i < backStackSize; i++) {
            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        return false;
    }

    @Override
    public final void dispose() {
        baseActivity = null;
    }

    @Override
    public void unauthorizedError(NetworkException exception) {

    }
}