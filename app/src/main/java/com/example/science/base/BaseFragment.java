package com.example.science.base;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.science.R;
import com.example.science.base.error.DefaultHttpErrorHandler;
import com.example.science.base.error.EmptyCommand;
import com.example.science.base.error.ErrorHandler;
import com.example.science.base.error.NetworkErrorHandler;
import com.example.science.base.error.UnauthorizedErrorHandler;
import com.example.science.base.error.UnknownErrorHandler;
import com.example.science.data.network.error.NetworkException;
import com.example.science.di.Injectable;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;

//import dagger.android.support.HasSupportFragmentInjector;

public abstract class BaseFragment<P extends IPresenter, N extends INavigator> extends Fragment
        implements IView, Injectable, HasAndroidInjector, IToolbar {

    @Inject
    DispatchingAndroidInjector<Object> fragmentDispatchingAndroidInjector;

    @Inject
    P presenter;
    @Inject
    N navigator;

    @BindView(R.id.toolbar_container)
    @Nullable
    public ViewGroup toolbarContainer;

    private Unbinder unbinder;

    protected int getToolbarResource() {
        return 0;
    }

    private void initToolbar() {
        if (toolbarContainer != null && getToolbarResource() != 0)
            updateToolbar(getLayoutInflater().inflate(getToolbarResource(), null));
    }

    private void updateToolbar(View inflate) {
        if (toolbarContainer != null) {
            toolbarContainer.removeAllViews();
            toolbarContainer.addView(inflate);
        }
        createToolbar(inflate);
    }

    @Override
    public void createToolbar(View toolbar) {

    }
    private ErrorDialog errorDialog;

    //TODO create ErrorDialog builder
    private final ErrorHandler unauthorizedErrorHandler = new UnauthorizedErrorHandler(new EmptyCommand() {
        @Override
        public void execute() {
            onAttachDialog();
            errorDialog =  new ErrorDialog(requireActivity(),
                    requireContext().getString(R.string.unauthorized_error));
            errorDialog.show();
        }
    });

    private final ErrorHandler defaultHttpErrorHandler = new DefaultHttpErrorHandler(_arg ->{
        onAttachDialog();
        errorDialog = new ErrorDialog(requireActivity(), _arg);
        errorDialog.show();
    });

    //TODO add retry option for network errors
    private final ErrorHandler networkErrorHandler = new NetworkErrorHandler(_arg ->{
        onAttachDialog();
        errorDialog = new ErrorDialog(requireActivity(), _arg);
        errorDialog.show();
    });

    private final ErrorHandler unknownErrorHandler = new UnknownErrorHandler(_arg ->{
        onAttachDialog();
        errorDialog = new ErrorDialog(requireActivity(), _arg);
        errorDialog.show();
    });

    private void onAttachDialog(){
        if(errorDialog!=null&&errorDialog.isShowing())
            errorDialog.dismiss();
    }

    {
        unauthorizedErrorHandler.setSuccessor(defaultHttpErrorHandler);
        defaultHttpErrorHandler.setSuccessor(networkErrorHandler);
        networkErrorHandler.setSuccessor(unknownErrorHandler);
    }

    @LayoutRes
    protected abstract int getLayoutResource();

    protected final P requirePresenter() {
        if (presenter == null)
            throw new IllegalStateException("Presenter is not attached!!!");
        return presenter;
    }

    @CallSuper
    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle arguments = getArguments();
        if (arguments != null) parseArguments(arguments);
    }

    protected void parseArguments(final Bundle _arguments) {
    }

    @CallSuper
    @SuppressWarnings("unchecked")
    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater,
                             @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        final View view = inflater.inflate(getLayoutResource(), container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        initToolbar();
//    }

    @CallSuper
    @Override
    public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            presenter.attach(this);
        } else {
            presenter.reAttach(this);
        }
        initToolbar();
        onViewReady(savedInstanceState);
        onViewReady();
    }

    protected final void hideSoftKeyboard() {
        final InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = requireActivity().getCurrentFocus();
        if (view == null) view = new View(requireActivity());
        if (imm != null) imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    protected abstract void onViewReady(final Bundle _savedInstanceState);

    protected void onViewReady() {

    }

    protected final ErrorHandler getDefaultErrorHandler() {
        return unauthorizedErrorHandler;
    }

//    @Override
//    public final AndroidInjector<Fragment> supportFragmentInjector() {
//        return fragmentDispatchingAndroidInjector;
//    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    @CallSuper
    @Override
    public void showError(final Throwable _throwable) {
        if (_throwable instanceof NetworkException) {
            NetworkException exception = (NetworkException) _throwable;
            if(exception.getResponse()!=null)
                onErrorNetworkException(exception);
            else defaultHttpErrorHandler.handleException(exception);
        }
    }

    private void onErrorNetworkException(NetworkException exception){
        switch (exception.getResponseCode()) {
            case 422:
                navigator.unauthorizedError(exception);
                unauthorizedErrorHandler.handleException(exception);
                break;
            default:
                defaultHttpErrorHandler.handleException(exception);
                break;
        }
    }

    @CallSuper
    @Override
    public void onDestroyView() {
        presenter.detach();
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
        super.onDestroyView();
    }

    @CallSuper
    @Override
    public void onDestroy() {
        navigator.dispose();
        super.onDestroy();
    }

    @SuppressWarnings("unchecked")
    protected final N getNavigator() {
        return navigator;
    }


    public final <T extends Fragment> void openScreen(final Class<T> _fragmentToShow,
                                                      final @Nullable Bundle _arguments) {
        openScreen(_fragmentToShow, true, _arguments);
    }

    public final <T extends Fragment> void openScreen(final Class<T> _fragmentToShow,
                                                      final boolean _addToBackStack,
                                                      final @Nullable Bundle _arguments) {
      /*  final Fragment currentFragment = getActivity().getSupportFragmentManager()
                .findFragmentById(getActivity().getFragmentContainerId());
        if (currentFragment != null && currentFragment.getClass().equals(_fragmentToShow)) return;*/
        final Fragment fragment;
        if (_arguments == null) {
            fragment = Fragment.instantiate(getActivity(), _fragmentToShow.getName());
        } else {
            fragment = Fragment.instantiate(getActivity(), _fragmentToShow.getName(), _arguments);
        }

        final FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager()
                .beginTransaction();
        if (_addToBackStack) {
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }
        fragmentTransaction.replace(((ViewGroup) getView().getParent()).getId(), fragment, fragment.getClass().getName());
        fragmentTransaction.commitAllowingStateLoss();
    }
}