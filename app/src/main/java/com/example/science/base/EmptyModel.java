package com.example.science.base;

import javax.inject.Inject;

public final class EmptyModel extends BaseModel {

    @Inject
    EmptyModel() { }
}