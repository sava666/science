package com.example.science.base;

import com.example.science.data.network.error.NetworkException;

public interface INavigator {
    boolean navigateBack();
    boolean navigateBack(final boolean _closeAll);
    void dispose();
    void unauthorizedError(NetworkException exception);
}
