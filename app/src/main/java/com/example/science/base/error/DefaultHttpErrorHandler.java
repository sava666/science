package com.example.science.base.error;


import com.example.science.data.network.error.NetworkException;

public final class DefaultHttpErrorHandler extends ErrorHandler<String> {

    public DefaultHttpErrorHandler(final Command<String> _performAction) {
        super(_performAction);
    }

    @Override
    public final void handleException(final Throwable _throwable) {
        super.handleException(_throwable);
        final NetworkException erpException = (NetworkException) _throwable;
        if (erpException.getKind() == NetworkException.Kind.HTTP) {
            performAction.execute(erpException.getErrorBodyAs(String.class));
        } else {
            if (successor != null) successor.handleException(_throwable);
        }
    }
}