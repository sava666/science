package com.example.science.base;


import androidx.annotation.NonNull;

public interface IPresenter<V extends IView, M extends IModel> {
    void attach(@NonNull final V _view);
    void reAttach(@NonNull final V _view);
    boolean isAttached();
    void detach();
}