package com.example.science.domain;


import com.example.science.domain.get.characteristics.GetCharacteristicsRepository;
import com.example.science.domain.get.characteristics.GetCharacteristicsRepositoryImpl;
import com.example.science.domain.get.metrics.GetMetricsRepository;
import com.example.science.domain.get.metrics.GetMetricsRepositoryImpl;
import com.example.science.domain.insert.characteristic.InsertCharacteristicRepository;
import com.example.science.domain.insert.characteristic.InsertCharacteristicRepositoryImpl;
import com.example.science.domain.insert.metric.InsertMetricRepository;
import com.example.science.domain.insert.metric.InsertMetricRepositoryImpl;

import javax.inject.Singleton;
import dagger.Binds;
import dagger.Module;

@Module
public abstract class RepositoryModule {

    @Singleton
    @Binds
    abstract GetCharacteristicsRepository provideGetCharacteristicsRepository(final GetCharacteristicsRepositoryImpl getCharacteristicsRepository);

    @Singleton
    @Binds
    abstract GetMetricsRepository provideGetMetricsRepository(final GetMetricsRepositoryImpl getMetricsRepository);

    @Singleton
    @Binds
    abstract InsertCharacteristicRepository provideInsertCharacteristicRepository(final InsertCharacteristicRepositoryImpl insertCharacteristicRepository);

    @Singleton
    @Binds
    abstract InsertMetricRepository provideInsertMetricRepository(final InsertMetricRepositoryImpl insertMetricRepository);
}