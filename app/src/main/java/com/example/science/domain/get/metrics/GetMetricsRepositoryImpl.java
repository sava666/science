package com.example.science.domain.get.metrics;

import com.example.science.domain.get.characteristics.GetCharacteristicsRepositoryImpl;
import com.example.science.room.dao.MetricDAO;
import com.example.science.room.model.MetricModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public final class GetMetricsRepositoryImpl implements GetMetricsRepository {
    private final MetricDAO metricDAO;

    @Inject
    GetMetricsRepositoryImpl(final MetricDAO _metricDAO){
        metricDAO = _metricDAO;
    }

    @Override
    public Single<List<MetricModel>> getMetricsList() {
        return metricDAO.getMetricList();
    }
}
