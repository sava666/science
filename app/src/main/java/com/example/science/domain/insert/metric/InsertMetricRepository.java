package com.example.science.domain.insert.metric;

import com.example.science.room.model.MetricModel;

public interface InsertMetricRepository {
    void insertMetric(MetricModel... metricModel);
}
