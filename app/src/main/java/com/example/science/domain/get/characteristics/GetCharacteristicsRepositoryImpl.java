package com.example.science.domain.get.characteristics;

import com.example.science.room.dao.CharacteristicDAO;
import com.example.science.room.model.CharacteristicModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public final class GetCharacteristicsRepositoryImpl implements GetCharacteristicsRepository {
    private final CharacteristicDAO characteristicDAO;

    @Inject
    GetCharacteristicsRepositoryImpl(final CharacteristicDAO _characteristicDAO){
        characteristicDAO = _characteristicDAO;
    }
    @Override
    public Single<List<CharacteristicModel>> getCharacteristicList(int id) {
        return characteristicDAO.getCharacteristicList(id);
    }
}
