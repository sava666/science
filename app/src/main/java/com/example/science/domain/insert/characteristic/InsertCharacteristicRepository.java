package com.example.science.domain.insert.characteristic;

import com.example.science.room.model.CharacteristicModel;

public interface InsertCharacteristicRepository {
    void insertCharacteristic(CharacteristicModel... characteristicModel);
}
