package com.example.science.domain.insert.characteristic;

import com.example.science.room.dao.CharacteristicDAO;
import com.example.science.room.model.CharacteristicModel;

import javax.inject.Inject;

public final class InsertCharacteristicRepositoryImpl implements InsertCharacteristicRepository {

    private final CharacteristicDAO characteristicDAO;

    @Inject
    InsertCharacteristicRepositoryImpl(final CharacteristicDAO _characteristicDAO){
        characteristicDAO = _characteristicDAO;
    }

    @Override
    public void insertCharacteristic(CharacteristicModel... characteristicModel) {
        characteristicDAO.insertCharacteristicItem(characteristicModel);
    }
}
