package com.example.science.domain.get.metrics;

import com.example.science.room.model.MetricModel;

import java.util.List;

import io.reactivex.Single;

public interface GetMetricsRepository {
    Single<List<MetricModel>> getMetricsList();
}
