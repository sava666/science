package com.example.science.domain.insert.metric;

import com.example.science.domain.insert.characteristic.InsertCharacteristicRepositoryImpl;
import com.example.science.room.dao.MetricDAO;
import com.example.science.room.model.MetricModel;

import javax.inject.Inject;

public final class InsertMetricRepositoryImpl implements InsertMetricRepository{

    private final MetricDAO metricDAO;

    @Inject
    InsertMetricRepositoryImpl(final MetricDAO _metricDAO){
        metricDAO = _metricDAO;
    }
    @Override
    public void insertMetric(MetricModel... metricModel) {
        metricDAO.insertMetricItem(metricModel);
    }
}
