package com.example.science.domain.get.characteristics;

import com.example.science.room.model.CharacteristicModel;

import java.util.List;

import io.reactivex.Single;

public interface GetCharacteristicsRepository {
    Single<List<CharacteristicModel>>getCharacteristicList(int id);
}
