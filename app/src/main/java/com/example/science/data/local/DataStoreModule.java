package com.example.science.data.local;

import com.example.science.data.local.data.DataStore;
import com.example.science.data.local.data.DataStoreImpl;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class DataStoreModule {

    @Binds
    @Singleton
    abstract DataStore provideDataStore(final DataStoreImpl _dataStore);


}
