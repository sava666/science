package com.example.science.data.network;

import android.content.Context;
import android.os.Build;
import android.os.LocaleList;

import com.example.science.BuildConfig;
import com.example.science.data.config.NetworkConfiguration;
import com.example.science.data.network.factory.NullOnEmptyConverterFactory;
import com.example.science.data.network.factory.RxJava2CallAdapterFactoryWrapper;
import com.example.science.di.qualifier.Mapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

@Module
public abstract class NetworkModule {

    @Mapper
    @Provides
    @Singleton
    static Gson provideMapperGson() {
        return new GsonBuilder()
                .serializeNulls()
                .setLenient()
                .create();
    }


    @Singleton
    @Provides
    public static OkHttpClient provideOkHttpClient(final NetworkConfiguration defaultNetworkConfig) {
        final OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder()
                .readTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES);
        for (Interceptor interceptor : defaultNetworkConfig.getInterceptors()) {
            okHttpBuilder.addInterceptor(interceptor);
        }

        okHttpBuilder.hostnameVerifier((hostname, session) -> true);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        okHttpBuilder.addInterceptor(interceptor);
        okHttpBuilder.addInterceptor(chain -> {
            Request original = chain.request();
            Request request = original.newBuilder()
                    .header("Content-Type", "application/json")
//                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .header("Accept", "application/json")
                    .header("Accept-Language", getLanguage())
                    //.removeHeader("Pragma")
                    .build();

            okhttp3.Response response = chain.proceed(request);
            response.cacheResponse();
            return response;
        });
        okHttpBuilder.sslSocketFactory(getSSLSocketFactory());
        final Cache cache = new Cache(defaultNetworkConfig.getCacheDirectory(),
                defaultNetworkConfig.getCacheSize());
        return okHttpBuilder.cache(cache)
                .build();
    }

    private static String getLanguage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return LocaleList.getDefault().toLanguageTags();
        } else {
            return Locale.getDefault().getLanguage();
        }
    }
    @Singleton
    @Provides
    static Retrofit provideBaseRetrofit(Context context, final OkHttpClient httpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASEURL)
                .client(httpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactoryWrapper.create(RxJava2CallAdapterFactory.create()))
                .build();
    }

   /* @Singleton
    @Provides
    static ApolloClient provideBaseApolloClient(final OkHttpClient okHttpClient, final NetworkConfiguration defaultNetworkConfig){
        return ApolloClient.builder()
                .serverUrl("https://dialik.com/graphql")
                .httpCache(new ApolloHttpCache(defaultNetworkConfig.getHttpCasheStore()))
                .okHttpClient(okHttpClient)
                .build();
    }*/

    public static SSLSocketFactory getSSLSocketFactory() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            return sslContext.getSocketFactory();
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            return null;
        }

    }

   /* @Singleton
    @Provides
    static GetDetailPlacesService provideGetDetailPlacesService(final Retrofit _retrofit){
        return _retrofit.create(GetDetailPlacesService.class);
    }

    @Singleton
    @Provides
    static GetLocationService provideGetLocationService(final Retrofit _retrofit){
        return _retrofit.create(GetLocationService.class);
    }

    @Singleton
    @Provides
    static GetLocationTomatoService provideGetLocationTomatoService(final Retrofit _retrofit){
        return _retrofit.create(GetLocationTomatoService.class);
    }
*/
    /*@Singleton
    @Provides
    static GetForecastService provideGetForecastService(final GetForecastServiceImpl _getForecastService, final ApolloClient _apolloClient){
        return _apolloClient.query(_getForecastService);
    }*/

}