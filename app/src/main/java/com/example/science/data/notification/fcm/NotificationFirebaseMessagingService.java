package com.example.science.data.notification.fcm;

import android.util.Log;

import com.example.science.data.local.data.DataStore;
import com.example.science.models.notification.DataNotification;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import io.reactivex.disposables.CompositeDisposable;

public class NotificationFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM";
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

//    @Inject
//    FcmDeviceCreateRepository fcmDeviceCreateRepository;

    @Inject
    DataStore dataStore;

    @Override
    public void onCreate() {
        AndroidInjection.inject(this);
        super.onCreate();
    }

    @Override
    public void onMessageReceived(@NotNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Gson gson = new Gson();
        Log.i("NotificationBean", "sendNotification: " + remoteMessage.getData());
        DataNotification dataNotification = gson.fromJson(gson.toJson(remoteMessage.getData()), DataNotification.class);
        NotificationBean.sendNotification(getApplicationContext(), dataNotification);
    }

    @Override
    public void onNewToken(@NotNull String s) {
        super.onNewToken(s);
        Log.d(TAG, "onNewToken: " + s);
        dataStore.setFCMToken(s);

//        compositeDisposable.add(fcmDeviceCreateRepository.apiFcmDeviceCreate(new FCM("android", dataStore.getIdDevice(), s)).subscribeOn(Schedulers.io())
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(devices -> {
//                            Log.i(TAG, "onNewToken: " + devices.toString());
//                            onDispose();
//                        },
//                        _throwable -> {
//                            Log.i(TAG, "onNewToken: " + _throwable.getMessage());
//                            onDispose();
//                        }));

    }

    private void onDispose() {
        if (!compositeDisposable.isDisposed())
            compositeDisposable.dispose();
    }
}
