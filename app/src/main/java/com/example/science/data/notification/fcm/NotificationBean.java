package com.example.science.data.notification.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.example.science.R;
import com.example.science.common.Keys;
import com.example.science.models.notification.DataNotification;
import com.example.science.presentation.splash.SplashActivity;

class NotificationBean {

    static void sendNotification(Context context, DataNotification dataNotification) {

        Intent intent = new Intent(context, SplashActivity.class);
        intent.putExtra(Keys.Args.TYPE, dataNotification.getType());
        if(dataNotification.getExtra()!=null)
        intent.putExtra(Keys.Args.EXTRA, dataNotification.getExtra());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                intent, PendingIntent.FLAG_ONE_SHOT);
        int notifyID = 1;
        String CHANNEL_ID = "kardio_01";// The id of the channel.
        CharSequence name = context.getString(R.string.app_name);// The user-visible name of the channel.

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            // The user-visible name of the channel.
            // The user-visible description of the channel.
            String description = "p.aid controls";
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            // Configure the notification channel.
            mChannel.setDescription(description);
            mChannel.setShowBadge(true);

            mChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(mChannel);
            }
        }
        Uri defaultSoundUri = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context, CHANNEL_ID)
//                        .setSmallIcon(R.mipmap.ic_launcher_foreground)
                        .setContentTitle(dataNotification.getTitle())
                        .setContentText(dataNotification.getBody())
                        .setAutoCancel(true)
//                        .setNumber(NOTIFICATION_COUNT<=0?0:NOTIFICATION_COUNT)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        if (mNotificationManager != null) {
            mNotificationManager.notify(notifyID, notificationBuilder.build());
        }

    }
}
