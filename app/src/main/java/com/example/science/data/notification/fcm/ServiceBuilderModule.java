package com.example.science.data.notification.fcm;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ServiceBuilderModule {

    @ContributesAndroidInjector
    abstract NotificationFirebaseMessagingService notificationFirebaseMessagingService();

}
