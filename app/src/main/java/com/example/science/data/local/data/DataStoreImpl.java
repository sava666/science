package com.example.science.data.local.data;

import android.content.SharedPreferences;
import android.util.Log;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public final class DataStoreImpl implements DataStore {

    //TODO clean this DataStore from unnecessary keys
    private final static String KEY_FCM_TOKEN = "data_fcm_token";
    private final static String KEY_IF_UPDATE = "data_if_update";
    private final static String KEY_CHARACTERISTIC_TITLE = "data_characteristic";
    private final static String KEY_FORMULA_ID = "data_formula_id";
    private final static String KEY_FORMULA_TITLE = "data_formula_title";
    private final static String KEY_RESULT = "data_result";


    private final SharedPreferences sharedPreferences;

    @Inject
    DataStoreImpl(final SharedPreferences _sharedPreferences) {
        sharedPreferences = _sharedPreferences;
    }


    @Override
    public void setIfUpdate(boolean state) {
        sharedPreferences.edit()
                .putBoolean(KEY_IF_UPDATE, state)
                .apply();
    }

    @Override
    public boolean getIfUpdate() {
        return sharedPreferences.getBoolean(KEY_IF_UPDATE ,true);
    }

    @Override
    public void setCharacteristicTitle(String characteristic) {
        sharedPreferences.edit()
                .putString(KEY_CHARACTERISTIC_TITLE, characteristic)
                .apply();
    }

    @Override
    public String getCharacteristicTitle() {
        return sharedPreferences.getString(KEY_CHARACTERISTIC_TITLE ,"");
    }


    @Override
    public void setFormulaId(int id) {
        sharedPreferences.edit()
                .putInt(KEY_FORMULA_ID, id)
                .apply();
    }

    @Override
    public int getFormulaId() {
        return sharedPreferences.getInt(KEY_FORMULA_ID ,0);
    }

    @Override
    public void setFormula(String id) {
        sharedPreferences.edit()
                .putString(KEY_FORMULA_TITLE, id)
                .apply();
    }

    @Override
    public String getFormula() {
        return sharedPreferences.getString(KEY_FORMULA_TITLE ,"");
    }

    @Override
    public void setResult(float result) {
        sharedPreferences.edit()
                .putFloat(KEY_RESULT, result)
                .apply();
    }

    @Override
    public float getResult() {
        return sharedPreferences.getFloat(KEY_RESULT ,0);
    }


    @Override
    public void setFCMToken(String value) {
        sharedPreferences.edit()
                .putString(KEY_FCM_TOKEN, value)
                .apply();
    }

}