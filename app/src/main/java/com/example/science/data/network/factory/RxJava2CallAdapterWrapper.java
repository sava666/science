package com.example.science.data.network.factory;

import com.example.science.data.network.error.NetworkException;

import java.io.IOException;
import java.lang.reflect.Type;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.HttpException;
import retrofit2.Response;
import retrofit2.Retrofit;

public final class RxJava2CallAdapterWrapper<R> implements CallAdapter<R, Object> {

    private Retrofit retrofit;
    private CallAdapter<R, Object> rObjectCallAdapter;

    RxJava2CallAdapterWrapper(final Retrofit _retrofit, final CallAdapter<R, Object> _rObjectCallAdapter) {
        retrofit = _retrofit;
        rObjectCallAdapter = _rObjectCallAdapter;
    }

    @Override
    public Type responseType() {
        return rObjectCallAdapter.responseType();
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object adapt(final Call<R> call) {
        final Object object = rObjectCallAdapter.adapt(call);
        if (object instanceof Completable) {
            return ((Completable) object).onErrorResumeNext(_throwable -> Completable.error(parseException(_throwable)));
        } else if (object instanceof Single) {
            return ((Single) object).onErrorResumeNext(_throwable -> Single.error(parseException((Throwable) _throwable)));
        } else if (object instanceof Maybe) {
            return ((Maybe) object).onErrorResumeNext(_throwable -> {
                return Maybe.error(parseException((Throwable) _throwable));
            });
        } else if (object instanceof Observable) {
            return ((Observable) object).onErrorResumeNext(_throwable -> {
                return Observable.error(parseException((Throwable) _throwable));
            });
        } else {
            return object;
        }
    }

    private NetworkException parseException(final Throwable _throwable) {
        // We had non-200 http error
        if (_throwable instanceof HttpException) {
            final HttpException httpException = (HttpException) _throwable;
            final Response response = httpException.response();
            return NetworkException.httpError(response.raw().request().url().toString(), response, retrofit);
        }
        // A network error happened
        if (_throwable instanceof IOException) {
            return NetworkException.networkError((IOException) _throwable);
        }
        // We don't know what happened. We need to simply convert to an unknown error
        return NetworkException.unexpectedError(_throwable);
    }
}
