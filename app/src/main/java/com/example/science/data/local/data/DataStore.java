package com.example.science.data.local.data;

public interface DataStore {


    void setIfUpdate(boolean state);
    boolean getIfUpdate();

    void setCharacteristicTitle(String cypher);
    String getCharacteristicTitle();

    void setFormulaId(int id);
    int getFormulaId();

    void setFormula(String id);
    String getFormula();

    void setResult(float result);
    float getResult();

    void setFCMToken(String value);

}