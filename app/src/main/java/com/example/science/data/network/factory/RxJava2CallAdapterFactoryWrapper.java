package com.example.science.data.network.factory;


import androidx.annotation.NonNull;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import retrofit2.CallAdapter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

public final class RxJava2CallAdapterFactoryWrapper extends CallAdapter.Factory {

    private RxJava2CallAdapterFactory rxJava2CallAdapterFactory;

    public static RxJava2CallAdapterFactoryWrapper create(final RxJava2CallAdapterFactory _rxJava2CallAdapterFactory) {
        return new RxJava2CallAdapterFactoryWrapper(_rxJava2CallAdapterFactory);
    }

    private RxJava2CallAdapterFactoryWrapper(@NonNull final RxJava2CallAdapterFactory _rxJava2CallAdapterFactory) {
        rxJava2CallAdapterFactory = _rxJava2CallAdapterFactory;
    }

    @SuppressWarnings("unchecked")
    @Override
    public CallAdapter<?, ?> get(@NonNull final Type returnType,
                                 @NonNull final Annotation[] annotations,
                                 @NonNull final Retrofit retrofit) {
        final CallAdapter<?, ?> callAdapter = rxJava2CallAdapterFactory.get(returnType, annotations, retrofit);
        if (callAdapter != null) {
            return new RxJava2CallAdapterWrapper(retrofit, callAdapter);
        } else {
            return null;
        }
    }
}