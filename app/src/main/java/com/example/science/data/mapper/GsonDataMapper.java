package com.example.science.data.mapper;

import com.example.science.di.qualifier.Mapper;
import com.google.gson.Gson;

import java.lang.reflect.Type;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public final class GsonDataMapper implements DataMapper {

    private final Gson gson;

    @Inject
    GsonDataMapper(@Mapper final Gson _gson) {
        gson = _gson;
    }

    @Override
    public <T> T fromString(final String _data, final Class<T> _tClass) {
        //TODO try this way
//        Type type = new TypeToken<T>(){}.getType();
//        return fromString(_data, type);
        return gson.fromJson(_data, _tClass);
    }

    @Override
    public <T> T fromString(final String _data, final Type _type) {
        return gson.fromJson(_data, _type);
    }

    @Override
    public <T> String toString(final T _data) {
        return gson.toJson(_data);
    }
}
