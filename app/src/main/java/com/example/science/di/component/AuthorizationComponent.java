package com.example.science.di.component;

import com.example.science.MyApplication;

public interface AuthorizationComponent {
    void inject(final MyApplication app);
}