package com.example.science.di.component;

import android.app.Application;

import com.example.science.MyApplication;
import com.example.science.data.local.DataStoreModule;
import com.example.science.data.network.NetworkModule;
import com.example.science.data.notification.fcm.ServiceBuilderModule;
import com.example.science.di.module.AppModule;
import com.example.science.domain.RepositoryModule;
import com.example.science.room.DataBaseModule;
import com.example.science.util.resolver.ResolverModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AndroidSupportInjectionModule.class,
        RepositoryModule.class,
        AppModule.class,
        NetworkModule.class,
        DataStoreModule.class,
        ServiceBuilderModule.class,
        ResolverModule.class,
        DataBaseModule.class
})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(final Application _application);
        AppComponent build();
    }

    void inject(final MyApplication _app);
}