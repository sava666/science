package com.example.science.di;

import android.content.Context;

import androidx.annotation.NonNull;

import com.example.science.MyApplication;
import com.example.science.di.component.AuthorizationComponent;
import com.example.science.di.module.AuthorizedComponent;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public final class ComponentHandler {

    private final AuthorizedComponent.Builder authorizedComponentBuilder;
    private final MyApplication app;

    private AuthorizationComponent authorizationComponent;

    @Inject
    ComponentHandler(@NonNull final AuthorizedComponent.Builder _authorizedComponentBuilder,
                     @NonNull final Context _context) {
        authorizedComponentBuilder = _authorizedComponentBuilder;
        app = (MyApplication) _context.getApplicationContext();
    }

    public final void buildRequiredComponent() {
        authorizationComponent = null;
        authorizationComponent = authorizedComponentBuilder
                .build();
        authorizationComponent.inject(app);
    }
}