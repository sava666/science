package com.example.science.di.module;

import com.example.science.presentation.main.MainActivity;
import com.example.science.presentation.main.MainModule;
import com.example.science.presentation.main.MainScope;
import com.example.science.presentation.splash.SplashActivity;
import com.example.science.presentation.splash.SplashModule;
import com.example.science.presentation.splash.SplashScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AuthorizedActivityModule {

    @SplashScope
    @ContributesAndroidInjector(modules = SplashModule.class)
    abstract SplashActivity splashActivity();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainActivity mainActivity();

}