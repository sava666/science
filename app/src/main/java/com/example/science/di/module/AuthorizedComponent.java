package com.example.science.di.module;

import com.example.science.di.component.AuthorizationComponent;
import com.example.science.di.scope.AuthorizedScope;

import dagger.Subcomponent;

@AuthorizedScope
@Subcomponent(modules = AuthorizedModule.class)
public interface AuthorizedComponent extends AuthorizationComponent {
    @Subcomponent.Builder
    interface Builder {
        AuthorizedComponent build();
    }
}