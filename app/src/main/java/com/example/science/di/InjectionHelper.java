package com.example.science.di;


import android.app.Activity;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.example.science.MyApplication;
import com.example.science.di.component.DaggerAppComponent;
import com.example.science.util.SimpleActivityLifecycleCallbacks;

import dagger.android.AndroidInjection;
import dagger.android.support.AndroidSupportInjection;

public final class InjectionHelper {

    public static void init(final MyApplication _application) {

        DaggerAppComponent.builder()
                .application(_application)
                .build()
                .inject(_application);
        registerActivityInjector(_application);
    }

    private static void registerActivityInjector(final MyApplication _application) {
        _application.registerActivityLifecycleCallbacks(new SimpleActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                injectActivity(activity);
            }
        });
    }

    private static void injectActivity(final Activity _activity) {
        if (_activity instanceof Injectable) {
            AndroidInjection.inject(_activity);
        }
        if (_activity instanceof FragmentActivity) {
            //TODO probably need to unregister when activity destroyed
            ((FragmentActivity) _activity).getSupportFragmentManager()
                    .registerFragmentLifecycleCallbacks(
                            new FragmentManager.FragmentLifecycleCallbacks() {
                                @Override
                                public void onFragmentCreated(FragmentManager fm, Fragment f,
                                                              Bundle savedInstanceState) {
                                    if (f instanceof Injectable) {
                                        AndroidSupportInjection.inject(f);
                                    }
                                }
                            }, true);
        }
    }
}