package com.example.science.di.module;

import com.example.science.di.scope.AuthorizedScope;
import com.example.science.presentation.main.MainContract;
import com.example.science.presentation.main.MainModel;
import com.example.science.presentation.splash.SplashContract;
import com.example.science.presentation.splash.SplashModel;

import dagger.Binds;
import dagger.Module;

@Module(includes = AuthorizedActivityModule.class)
public abstract class AuthorizedModule {

    @AuthorizedScope
    @Binds
    abstract SplashContract.AbsModel provideSplashModel(final SplashModel categoriesModel);

    @AuthorizedScope
    @Binds
    abstract MainContract.AbsModel provideMainModel(final MainModel categoriesModel);

}