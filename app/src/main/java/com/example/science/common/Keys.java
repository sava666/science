package com.example.science.common;



public interface Keys {
    interface Args {
        String AUTH = "AUTH";
        String USER = "USER";
        String SOCIAL = "SOCIAL";
        String NEW_USER_TO_PHONE = "NEW_USER_TO_PHONE";
        String FACEBOOK_TOKEN = "FACEBOOK_TOKEN";
        String ID = "id";
        String DATA = "DATA";
        String LIST_CONTACT = "LIST_CONTACT";
        String CHAT = "CHAT";
        String FORM = "form";
        String TYPE = "type";
        String EXTRA = "extra";
        String BOOKING_SERVICE = "BOOKING_SERVICE";
        String BOOKING_DATA_AND_ITEM = "BOOKING_DATA_AND_ITEM";
    }

    class Extras {
        public static int COUNT_PAGING = 50;
        public static int NOTIFICATION_COUNT = 0;
    }

    interface Constant {
        String HEADER_KEY = "Api-key";
        String HEADER_TOKEN_KEY = "Authorization";
        String HEADER_TOKEN_VALUE = "Token ";
        String HEADER_VALUE = "5029e560-4eae-4451-9ec6-89e7af1d52d8";
        String ID = "id";
        String KEY_ID = "id";
        String FCM = "data";
        String DATA = "data";
        String DATE = "date";

        String ORDERING = "ordering";
        String LIMIT = "limit";
        String SEARCH = "search";
        String OFFSET = "offset";
        String IS_READ = "is_read";
        String CREATED__GTE = "created__gte";
        String CREATED__LTE = "created__lte";
        String CREATED__LT = "created__lt";
        String DATE__GTE = "date__gte";
        String DATE__LTE = "date__lte";

        String ROOM_ID = "room_id";
        String FORM = "form";
        String TYPE = "type";

        String BADGE = "badge";

       String INFO_UPDATE_NOTIFICATION = "INFO_UPDATE_NOTIFICATION";
       String INFO_UPDATE_NOTIFICATION_BADGE = "INFO_UPDATE_NOTIFICATION_BADGE";
    }

}