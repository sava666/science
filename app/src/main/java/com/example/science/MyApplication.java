package com.example.science;

import android.app.Application;

import com.example.science.di.ComponentHandler;
import com.example.science.di.InjectionHelper;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;

//import com.crashlytics.android.Crashlytics;

/**
 * Created by roman on 25.05.17.
 */
public class MyApplication extends Application implements HasAndroidInjector {

    @Inject
    protected DispatchingAndroidInjector<Object> dispatchingAndroidInjector;

    @Inject
    protected ComponentHandler componentHandler;

    private static MyApplication INSTANCE;

    @Override
    public void onCreate() {
        super.onCreate();
        InjectionHelper.init(this);

        INSTANCE = this;
        componentHandler.buildRequiredComponent();
    }

    public static MyApplication getInstance() {
        return INSTANCE;
    }

//    @Override
//    public AndroidInjector<Activity> activityInjector() {
//        return dispatchingActivityInjector;
//    }
//
//    @Override
//    public AndroidInjector<Service> serviceInjector() {
//        return dispatchingServiceInjector;
//    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return dispatchingAndroidInjector;
    }
}