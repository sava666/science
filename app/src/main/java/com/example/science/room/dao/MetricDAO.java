package com.example.science.room.dao;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.science.room.model.MetricModel;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface MetricDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMetricItem(MetricModel... metricModels);

    @Query("SELECT * FROM metric")
    Single<List<MetricModel>> getMetricList();
}
