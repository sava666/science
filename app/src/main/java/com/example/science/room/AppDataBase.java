package com.example.science.room;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.science.room.dao.CharacteristicDAO;
import com.example.science.room.dao.MetricDAO;
import com.example.science.room.model.CharacteristicModel;
import com.example.science.room.model.MetricModel;

import javax.inject.Singleton;

@Database(entities = {MetricModel.class, CharacteristicModel.class}, version = 1, exportSchema = false)
@Singleton
public abstract class AppDataBase extends RoomDatabase {
    public abstract MetricDAO getMetricDAO();
    public abstract CharacteristicDAO getCharacteristicDAO();
}
