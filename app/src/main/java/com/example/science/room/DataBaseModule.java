package com.example.science.room;

import android.content.Context;

import androidx.room.Room;

import com.example.science.room.dao.CharacteristicDAO;
import com.example.science.room.dao.MetricDAO;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class DataBaseModule {
    @Provides
    @Singleton
    static AppDataBase provideAppDataBase(final Context context){
        return Room.databaseBuilder(context, AppDataBase.class, "science.db").fallbackToDestructiveMigration()
                .allowMainThreadQueries().build();
    }

    @Provides
    @Singleton
    static MetricDAO provideHistoryDao(AppDataBase appDataBase){
        return appDataBase.getMetricDAO();
    }

    @Provides
    @Singleton
    static CharacteristicDAO provideCharacteristicDao(AppDataBase appDataBase){
        return appDataBase.getCharacteristicDAO();
    }
}
