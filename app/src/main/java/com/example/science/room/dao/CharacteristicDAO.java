package com.example.science.room.dao;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.science.room.model.CharacteristicModel;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface CharacteristicDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCharacteristicItem(CharacteristicModel... metricModels);

    @Query("SELECT * FROM characteristics WHERE metric_id=:id")
    Single<List<CharacteristicModel>> getCharacteristicList(int id);
}
