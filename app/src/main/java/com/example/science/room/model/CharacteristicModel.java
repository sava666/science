package com.example.science.room.model;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.science.models.retrofit.core.Model;

@Entity(tableName = "characteristics")
public class CharacteristicModel extends Model {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "metric_id")
    private int metricId;

    @ColumnInfo(name = "characteristic_name")
    private String characteristic;

    @ColumnInfo(name = "characteristic_cypher")
    private String sypher;

    public CharacteristicModel(int metricId, String characteristic, String sypher) {
        this.metricId = metricId;
        this.characteristic = characteristic;
        this.sypher = sypher;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMetricId() {
        return metricId;
    }

    public void setMetricId(int metricId) {
        this.metricId = metricId;
    }

    public String getCharacteristic() {
        return characteristic;
    }

    public void setCharacteristic(String characteristic) {
        this.characteristic = characteristic;
    }

    public String getSypher() {
        return sypher;
    }

    public void setSypher(String sypher) {
        this.sypher = sypher;
    }
}
