package com.example.science.util.resolver;
import com.example.science.util.resolver.base.ResolverImpl;
import com.example.science.util.resolver.base.ResolverInterface;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ResolverModule {

    @Binds
    @Singleton
    abstract ResolverInterface provideGetLocation(final ResolverImpl _getResolver);
}
