package com.example.science.util;

import android.content.Context;
import android.util.DisplayMetrics;

public class UtilFunction {

    public static String getPhone(String phone){
        return phone.replaceAll("[\\s()]", "");
    }

    public static float convertDpToPixel(float dp, Context context){
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static String setTextToUpperCase(String text){
        StringBuilder sb = new StringBuilder(text);
        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        return sb.toString();
    }
}
