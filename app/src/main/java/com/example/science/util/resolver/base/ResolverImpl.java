package com.example.science.util.resolver.base;

import android.content.Context;

import com.example.science.R;
import com.example.science.data.local.data.DataStore;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public final class ResolverImpl implements ResolverInterface{

    @Inject
    DataStore dataStore;

    @Inject
    ResolverImpl(){
    }

    @Override
    public String resolveFormula(String cypher, Context context) {
        switch (cypher) {
            case "Ф201":
            case "Ф202":
            case "Ф203":
            case "Ф302":
            case "Ф403":
            case "З301":
            case "З303":
            case "З305":
            case "Е104":
            case "Е203":
            case "Е208":
            case "Е212":
            case "П301":
            case "С301":{
                dataStore.setFormulaId(1);
                return context.getResources().getString(R.string.formula_one);
            }

            case "Н106":
            case "Н302":
            case "З308":{
                dataStore.setFormulaId(2);
                return context.getResources().getString(R.string.formula_two);
            }
            case "Н301":{
                dataStore.setFormulaId(3);
                return context.getResources().getString(R.string.formula_three);
            }
            case "З201":
            case "З202":
            case "З302":
            case "Е101":
            case "Е107":
            case "Е205":
            case "П104":{
                dataStore.setFormulaId(4);
                return context.getResources().getString(R.string.formula_four);
            }
            case "Е102":
            case "Е103":
            case "Е108":
            case "Е109":
            case "Е110":
            case "Е201":
            case "Е210":{
                dataStore.setFormulaId(5);
                return context.getResources().getString(R.string.formula_five);
            }
            case "Н303":
            case "С402":
            case "С104":
            case "С201":
            case "С202":
            case "С203":{
                dataStore.setFormulaId(6);
                return context.getResources().getString(R.string.formula_six);
            }
            case "Е105":
            case "Е106":{
                dataStore.setFormulaId(7);
                return context.getResources().getString(R.string.formula_seven);
            }
            default:
                return context.getResources().getString(R.string.formula_error);
        }
    }

    @Override
    public String resolveDescription(String cypher, Context context) {
        switch (cypher) {
            case "Ф201":
                return context.getResources().getString(R.string.expected_accuracy_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.expected_accuracy_t);
            case "Ф202":
                return context.getResources().getString(R.string.сomputational_correctness_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.сomputational_correctness_t);
            case "Ф203":
                return context.getResources().getString(R.string.accuracy_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.accuracy_t);
            case "Ф302":
                return context.getResources().getString(R.string.data_sharing_ability_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.data_sharing_ability_t);
            case "Ф403":
                return context.getResources().getString(R.string.data_corruption_prevention_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.data_corruption_prevention_t);
            case "З301":
                return context.getResources().getString(R.string.operational_compliance_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.operational_compliance_t);
            case "З303":
                return context.getResources().getString(R.string.error_correction_during_usage_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.error_correction_during_usage_t);
            case "З305":
                return context.getResources().getString(R.string.messages_understability_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.messages_understability_t);
            case "Е104":
                return context.getResources().getString(R.string.capacity_a);
            case "Е203":
                return context.getResources().getString(R.string.errors_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.errors_t);
            case "Е208":
                return context.getResources().getString(R.string.errors_number_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.errors_number_t);
            case "Е212":
                return context.getResources().getString(R.string.average_erroes_number_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.average_erroes_number_t);
            case "П301":
                return context.getResources().getString(R.string.coexisting_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.coexisting_t);
            case "С301":
                return context.getResources().getString(R.string.change_success_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.change_success_t);
            case "Н106":
                return context.getResources().getString(R.string.mtbf_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.mtbf_t);
            case "Н302":
                return context.getResources().getString(R.string.average_downtime_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.average_downtime_t);
            case "З308":
                return context.getResources().getString(R.string.time_between_operations_with_mistakes_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.time_between_operations_with_mistakes_t);
            case "Н301":
                return context.getResources().getString(R.string.readiness_t)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.readiness_t_a);
            case "Е102":
                return context.getResources().getString(R.string.average_response_time_t)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.average_response_time_t_a);
            case "Е103":
                return context.getResources().getString(R.string.worst_response_time_factor_t)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.worst_response_time_factor_t_a);
            case "Е108":
                return context.getResources().getString(R.string.average_request_time_t)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.average_request_time_t_a);
            case "Е109":
                return context.getResources().getString(R.string.worst_request_time_factor_t)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.worst_request_time_factor_t_a);
            case "Е110":
                return context.getResources().getString(R.string.waiting_time_t)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.waiting_time_t_a);
            case "Е201":
                return context.getResources().getString(R.string.device_workload_t)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.device_workload_t_a);
            case "Е210":
                return context.getResources().getString(R.string.balancing_media_usage_t)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.balancing_media_usage_t_a);
            case "Н303":
                return context.getResources().getString(R.string.average_recovery_time_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.average_recovery_time_t);
            case "С402":
                return context.getResources().getString(R.string.retesting_effectiveness_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.retesting_effectiveness_t);
            case "С104":
                return context.getResources().getString(R.string.failure_analysis_efficiency_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.failure_analysis_efficiency_t);
            case "С201":
                return context.getResources().getString(R.string.change_cycle_efficiency_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.change_cycle_efficiency_t);
            case "С202":
                return context.getResources().getString(R.string.change_implementation_duration_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.change_implementation_duration_t);
            case "С203":
                return context.getResources().getString(R.string.modifying_difficulty_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.modifying_difficulty_t);
            case "Е105":
                return context.getResources().getString(R.string.average_capacity_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.average_capacity_t);
            case "Е106":
                return context.getResources().getString(R.string.worst_capacity_factor_a)+
                        context.getResources().getString(R.string.enter)+
                        context.getResources().getString(R.string.worst_capacity_factor_t);
            default:
                return context.getResources().getString(R.string.description_error);
        }
    }
}
