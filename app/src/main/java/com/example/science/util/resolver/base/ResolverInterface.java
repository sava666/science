package com.example.science.util.resolver.base;

import android.content.Context;
import android.widget.LinearLayout;

import com.example.science.data.local.data.DataStore;

public interface ResolverInterface {

    String resolveFormula(String cypher, Context context);
    String resolveDescription(String cypher, Context context);

}
