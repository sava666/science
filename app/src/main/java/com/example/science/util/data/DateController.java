package com.example.science.util.data;


import android.util.Log;

import com.annimon.stream.Stream;
import com.example.science.util.data.core.ConstantData;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


/**
 * Created by hanz on 25.04.2018.
 */

public class DateController {

    public static Locale getLocale() {
        return new Locale(ConstantData.LOCALE_LANGUAGE, ConstantData.COUNTRY);
    }

    public static String formatDate(String data, String oldFormatData, String newFormatData, TimeZone timeZone, Locale locale) {
        if (data == null || data.isEmpty())
            return ConstantData.NullableDAta;
        else {
            if (locale == null) locale = getLocale();
            SimpleDateFormat simpleDateFormat;
            Date dt = null;
            try {
                simpleDateFormat = new SimpleDateFormat(oldFormatData, locale);
                if (timeZone != null)
                    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                dt = simpleDateFormat.parse(data);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat(newFormatData, locale);
            if (timeZone != null)
                dateFormat.setTimeZone(timeZone);
            return dateFormat.format(dt);
        }
    }

    public static String formatDate(String data, String oldFormatData, String newFormatData, Locale locale) {
        return formatDate(data, oldFormatData, newFormatData, null, locale);
    }

    public static String formatDate(String data, String oldFormatData, String newFormatData, TimeZone timeZone) {
        return formatDate(data, oldFormatData, newFormatData, timeZone, null);
    }

    public static String formatDate(String data, String oldFormatData, String newFormatData) {
        return formatDate(data, oldFormatData, newFormatData, null, null);
    }

    public static Integer formatDateInt(String data, String oldFormatData, String newFormatData) {
        if (data == null || data.isEmpty())
            return null;
        else {
            Date dt = null;
            try {
                dt = new SimpleDateFormat(oldFormatData, getLocale()).parse(data);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat(newFormatData, getLocale());
            Log.e("formatDateInt", data + " =" + oldFormatData + "  " + newFormatData + " " + Integer.valueOf(dateFormat.format(dt)));
            return Integer.valueOf(dateFormat.format(dt));
        }
    }

    public static String getDateProfile(int year1, int monthOfYear, int dayOfMonth) {
        DecimalFormat mFormat = new DecimalFormat("00");
        mFormat.setRoundingMode(RoundingMode.DOWN);
        Date jud = null;
        try {
            jud = new SimpleDateFormat(ConstantData.YYYY_MM_DD,
                    getLocale()).parse(year1 + "-"
                    + mFormat.format(Double.valueOf(monthOfYear + 1)) + "-" + mFormat.format(Double.valueOf(dayOfMonth)));
            return DateFormat.getDateInstance(SimpleDateFormat.LONG, getLocale()).format(jud).replace("г.", "");

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ConstantData.NullableDAta;
    }

    public static String getCalendarProfile(int year1, int monthOfYear, int dayOfMonth) {
        DecimalFormat mFormat = new DecimalFormat("00");
        mFormat.setRoundingMode(RoundingMode.DOWN);
        return String.format("%d-%s-%s", year1, mFormat.format(Double.valueOf(monthOfYear)), mFormat.format(Double.valueOf(dayOfMonth)));
    }

    public static String getCalendarBooking(int dayOfMonth, int monthOfYear,  int year1) {
        DecimalFormat mFormat = new DecimalFormat("00");
        mFormat.setRoundingMode(RoundingMode.DOWN);
        return String.format("%s.%s.%d", mFormat.format(Double.valueOf(dayOfMonth)), mFormat.format(Double.valueOf(monthOfYear)), year1);
    }

    public static String getTime(int hour, int minute) {
        DecimalFormat mFormat = new DecimalFormat("00");
        mFormat.setRoundingMode(RoundingMode.DOWN);
        return String.format("%s:%s", mFormat.format(hour), mFormat.format(minute));
    }

    public static String getDateApi(String data) {
        if (data == null || data.isEmpty())
            return ConstantData.NullableDAta;
        else {
            Date dt = null;
            try {
                dt = new SimpleDateFormat(ConstantData.YYYY_MM_DD, getLocale()).parse(data);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return DateFormat.getDateInstance(SimpleDateFormat.LONG, getLocale()).format(dt).replace("г.", "");
        }
    }

    public static Calendar getDate(String data,String formatData) {
            Date dt = null;
            try {
                dt = new SimpleDateFormat(formatData, getLocale()).parse(data);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        Calendar calendar = Calendar.getInstance();
        if (dt != null) {
            calendar.setTime(dt);
        }
        return calendar;
    }

    public static ArrayList<String> getWeekDayName() {
        DateFormatSymbols dfs = new DateFormatSymbols(Locale.getDefault());
        return Stream.of(dfs.getWeekdays()).filter(value -> !value.isEmpty())
                .collect(ArrayList::new, (list, value2) ->
                        list.add(value2.substring(0, 1).toUpperCase()));
    }

}
