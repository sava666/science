package com.example.science.util.data.core;

/**
 * Created by VeleS_Semecky on 08.02.2019.
 * velessemecky@gmail.com
 */
public interface ConstantData {
    String YYYY_MM_DD = "yyyy-MM-dd";
    String YYYY_MM_DD_T_HHcMMcSSpSSS_Z = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    String LLLL = "LLLL";
    String YYYYpMMpDDsHHcMM = "dd.MM.yyyy HH:mm";
    String YYYYpMMpDD = "dd.MM.yyyy";
    String DDsMMMMsYYYY = "d MMMM yyyy";
    String EEEE = "EEEE";
    String LOCALE_LANGUAGE = "uk";
    String COUNTRY = "UA";
    String HHcMMcSS = "HH:mm:ss";
    String HHcMM = "HH:mm";
    String NullableDAta = "-";
    String NullableDAtaInt = "0";
    String YYYY = "yyyy";
    String MM = "MM";
    String DD = "dd";
    String HH = "HH";
    String mm = "mm";

    String FAHRENHEIT = "fahrenheit";
    String CELSIUS = "celsius";
    int LOCATION_PERMISSION_ID = 1;
}
