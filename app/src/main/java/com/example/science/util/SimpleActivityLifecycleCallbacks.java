package com.example.science.util;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import org.jetbrains.annotations.NotNull;

public abstract class SimpleActivityLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {
    @Override
    public void onActivityCreated(@NotNull final Activity activity, final Bundle savedInstanceState) { }

    @Override
    public void onActivityStarted(@NotNull final Activity activity) { }

    @Override
    public void onActivityResumed(@NotNull final Activity activity) { }

    @Override
    public void onActivityPaused(@NotNull final Activity activity) { }

    @Override
    public void onActivityStopped(@NotNull final Activity activity) { }

    @Override
    public void onActivitySaveInstanceState(@NotNull final Activity activity, @NotNull final Bundle outState) { }

    @Override
    public void onActivityDestroyed(@NotNull final Activity activity) { }
}